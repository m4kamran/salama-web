import { Routes } from '@angular/router';
import { AuthGuard } from './core/guard/auth.guard';

//Componenets
import { BlankComponent, ExecutiveLayout } from './shared/layouts';
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { ParchiComponent } from './pages/parchi/parchi.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: ExecutiveLayout,
    children: [
      {
        path: '',
        loadChildren: './pages/admin/admin.module#AdminModule'
      }
    ]
  },
  {
    path: '',
    component: BlankComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'payment/:id',
        component: PaymentComponent
      },
      {
        path: 'parchi/:id',
        component: ParchiComponent
      },
      {
        path: '**',
        component: NotFoundComponent
      }
    ]
  }
];
