import { NgZone } from '@angular/core';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { switchMap, first } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { auth } from 'firebase/app';
import Admin from '../models/Admin';

@Injectable({ providedIn: 'root' })
export class AuthService {
  user: Observable<firebase.User>;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private ngZone: NgZone,
    private afs: AngularFirestore
  ) {
    this.user = this.afAuth.authState.switchMap(user => {
      if (user) {
        return this.afs.doc<Admin>(`admins/${user.uid}`).valueChanges();
      } else {
        return Observable.of(null);
      }
    });
  }

  signIn(email: string, password: string) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(res => {
      this.ngZone.run(() => this.router.navigate(['/dashboard']));
    });
  }

  signOut() {
    this.afAuth.auth.signOut().then(res => {
      this.ngZone.run(() => this.router.navigateByUrl('/login'));
    });
  }

  async getUser() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }
}
