export default interface Admin {
  name?: string;
  email?: string;
  fcmToken?: string;
}
