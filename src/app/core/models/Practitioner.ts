import HumanName from './DataTypes/HumanName';
import ContactPoint from './DataTypes/ContactPoint';
import Address from './DataTypes/Address';
import Gender from './DataTypes/Gender';
import Attachment from './DataTypes/Attachment';
import Language from './DataTypes/Language';
import Identifier from './DataTypes/Identifier';

export default interface Practitioner {
  identifier: Identifier;
  active: Boolean;
  name: [HumanName];
  telecom?: [ContactPoint];
  address?: [Address];
  gender?: Gender;
  birthDate?: Date;
  photo?: [Attachment];
  qualification?: string;
  communication?: Language; //FHIR Ends HERE
  cnic?: String;
  email?: String;
  password?: String;
  displayPicture?: String;
  login?: boolean;
  bio?: String;
  department?: String;
}
