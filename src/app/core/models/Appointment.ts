export default interface Appointment {
  appointmentType?: string;
  start?: Date;
  end?: Date;
  status?: string;
  serviceCategory?: string;
  description?: string;
  userId?: string;
  userName?: string;
  practitionerId?: string;
}
