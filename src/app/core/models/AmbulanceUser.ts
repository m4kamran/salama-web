import Person from './Person';
interface Location {
  position: {
    longitude?: Number;
    latitude?: Number;
    altitude?: Number;
  };
}

export default interface AmbulanceUser extends Person {
  location?: Location;
  email?: string;
  password?: string;
  login?: boolean;
  loc?: number[];
}
