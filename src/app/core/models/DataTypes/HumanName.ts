import Period from './Period';

enum UseTypes {
  usual = 'USUAL',
  official = 'OFFICIAL',
  temp = 'TEMP',
  nickname = 'NICKNAME',
  anonymous = 'ANONYMOUS',
  old = 'OLD',
  maiden = 'MAIDEN'
}

export default interface HumanName {
  use?: UseTypes;
  text: String;
  family?: String;
  given?: [String];
  prefix?: [String];
  suffix?: [String];
  period?: Period;
}
