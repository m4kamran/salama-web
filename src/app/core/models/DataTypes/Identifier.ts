import Period from './Period';

enum UseTypes {
  usual = 'USUAL',
  official = 'OFFICIAL',
  temp = 'TEMP',
  secondary = 'SECONDARY',
  old = 'OLD'
}

export default interface Identifier {
  use?: UseTypes;
  type?: String; // Needs to be changed as FHIR
  system?: String; //URI on FHIR, using String alternative in TypeScript
  value: String;
  period?: Period;
  assigner?: String; // Needs to be changed as FHIR as its Reference
}
