import Language from './Language';
enum MimeType {
  png = 'PNG',
  jpg = 'JPG',
  jpeg = 'JPEG'
}

export default interface Attachment {
  contentType?: MimeType; // Mime type of the content, with charset etc.
  language?: Language;
  data?: String; // Data inline, base64
  url: String; // Uri where the data can be found
  size?: Number; // Number of bytes of content (if url provided)
  hash?: String; // Hash of the data (sha-1, base64ed)
  title?: String; // Label to display in place of the data (alt)
  creation: Date; // Date attachment was first created
}
