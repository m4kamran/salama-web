import Period from './Period';

enum SystemType {
  phone = 'PHONE',
  fax = 'FAX',
  email = 'EMAIL',
  pager = 'PAGER',
  url = 'URL',
  sms = 'SMS',
  other = 'OTHER'
}

enum UseType {
  home = 'HOME',
  work = 'WORK',
  temp = 'TEMP',
  old = 'OLD',
  mobile = 'MOBILE'
}

export default interface ContactPoint {
  system: SystemType;
  value: String;
  use: UseType;
  rank: Number;
  period: Period;
}
