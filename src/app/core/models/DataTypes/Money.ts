export default interface Money {
  value: Number; // Numerical value (with implicit precision)
  currency?: String; //Needs to be changed as FHIR
}
