import HumanName from './HumanName';
import ContactPoint from './ContactPoint';
import Address from './Address';
import Gender from './Gender';
import Period from './Period';

export default interface Contact {
  relationship?: String; //Needs to be changed according to FHIR
  name: HumanName;
  telecom?: ContactPoint;
  address: Address;
  gender: Gender;
  organization: String; //Needs to be changed according to FHIR,
  period: Period;
}
