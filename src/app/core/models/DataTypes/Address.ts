import Period from './Period';

enum UseType {
  home = 'HOME',
  work = 'WORK',
  temp = 'TEMP',
  old = 'OLD',
  billing = 'BILLING'
}

enum TypeType {
  postal = 'POSTAL',
  physical = 'PHYSICAL'
}

export default interface Address {
  use: UseType;
  type: TypeType;
  text: String;
  line: [String]; // Street name, number, direction & P.O. Box etc.
  city: String;
  district: String;
  state: String;
  postalCode: String;
  country: String;
  period: Period;
}
