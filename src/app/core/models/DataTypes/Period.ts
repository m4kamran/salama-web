export default interface Period {
  start: Date;
  end: Date;
}
