export default interface Auth {
  uid?: string;
  email?: string;
  password?: string;
}
