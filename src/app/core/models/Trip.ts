export default interface Trip {
  to?: string;
  from?: string;
  duration?: string;
  distance?: string;
  ambualnceUserId?: string;
  patient?: string;
}
