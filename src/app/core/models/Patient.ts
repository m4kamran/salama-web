import HumanName from './DataTypes/HumanName';
import ContactPoint from './DataTypes/ContactPoint';
import Gender from './DataTypes/Gender';
import Address from './DataTypes/Address';
import Attachment from './DataTypes/Attachment';
import Period from './DataTypes/Period';
import Language from './DataTypes/Language';
import Identifier from './DataTypes/Identifier';
import Contact from './DataTypes/Contact';

interface Location {
  position: {
    longitude?: Number;
    latitude?: Number;
    altitude?: Number;
  };
}

interface Condition {
  level?: Number;
  description?: string;
  category?: string;
  picture?: string;
}

export default interface Patient {
  identifier?: Identifier;
  active?: boolean;
  name?: [HumanName];
  telecom?: [ContactPoint];
  gender?: Gender;
  birthDate?: Date;
  deceasedBoolean?: Boolean;
  deceasedDateTime?: Date;
  address?: Address;
  maritalStatus?: Boolean; // Boolean Temp. FHIR Standard {CodeableConcept} https://www.hl7.org/fhir/datatypes.html#CodeableConcept
  multipleBirthBoolean?: Boolean;
  multipleBirthInteger?: Number;
  photo?: [Attachment];
  contact?: Contact;
  communication?: [
    {
      language: Language;
      preffered: Boolean;
    }
  ];
  generalPractitioner?: String; //Needs to be discussed as its reference to 3 things
  managingOrganization?: String; //Needs to be discussed as its reference
  link?: String; //Needs to be discussed as its reference
  cnic?: String; // Does not Exist in FHIR
  condition?: Condition;
  location?: Location;
  status?: string;
  distance?: string;
  time?: string;
  place?: string;
  date?: Date;
  practitionerId?: string;
  practitionerName?: string;
  invoiceId?: string;
}
