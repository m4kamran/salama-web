import Identifier from './DataTypes/Identifier';
import Money from './DataTypes/Money';

export enum statusTypes {
  draft = 'DRAFT',
  issued = 'ISSUED',
  balanced = 'BALANCED',
  cancelled = 'CANCELLED',
  error = 'ENTERED-IN-ERROR'
}

enum priceComponentTypes {
  base = 'BASE',
  surcharge = 'SURCHARGE',
  deduction = 'DEDUCTION',
  discount = 'DISCOUNT',
  tax = 'TAX',
  informational = 'INFORMATIONAL'
}

export default interface Invoice {
  identifier?: [Identifier];
  status?: statusTypes;
  patientName?: string;
  // cancelledReason: String,
  //   type?: String;
  // subject?: { Reference(Patient|Group) },
  recipient?: String;
  date?: Date;
  // participant : [{
  //   role : { CodeableConcept },
  //   actor : { Reference(Practitioner|Organization|Patient|PractitionerRole|
  // }],
  issuer?: String;
  // account?: { Reference(Account) },
  lineItem?: [
    {
      sequence: Number;
      //   "chargeItemReference" : { Reference(ChargeItem) },
      //   "chargeItemCodeableConcept" : { CodeableConcept },
      priceComponent: [
        {
          type: priceComponentTypes;
          code?: String;
          factor?: Number;
          amount?: Money;
        }
      ];
    }
  ];
  // totalPriceComponent : [{ Content as for Invoice.lineItem.priceComponent }],
  totalNet?: Money;
  //   totalGross: Money; // Gross total of this Invoice
  // paymentTerms: "<markdown>", // Payment details
  note?: String;
}
