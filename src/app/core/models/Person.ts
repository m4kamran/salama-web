import Identifier from './DataTypes/Identifier';
import HumanName from './DataTypes/HumanName';
import ContactPoint from './DataTypes/ContactPoint';
import Gender from './DataTypes/Gender';
import Address from './DataTypes/Address';
import Attachment from './DataTypes/Attachment';

export default interface Person {
  identifier?: Identifier;
  name?: HumanName;
  telecom: [ContactPoint];
  gender: Gender;
  birthDate: Date;
  address: [Address];
  photo?: Attachment;
  active: Boolean;
  managingOrganization: String; //Needs to be changed as FHIR, Its Reference
}
