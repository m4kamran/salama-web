import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageService } from '../../shared/components/message/message.service';
import AmbulanceUser from '../models/AmbulanceUser';
import Practitioner from '../models/Practitioner';
import Auth from '../models/Auth';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import Trip from '../models/Trip';
import Appointment from '../models/Appointment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  AmbulanceUsersCollection: AngularFirestoreCollection<AmbulanceUser>;
  PractitionersCollection: AngularFirestoreCollection<Practitioner>;
  TripsCollection: AngularFirestoreCollection<Trip>;
  AppointmentsCollection: AngularFirestoreCollection<Appointment>;
  AmbulanceUserDocument: AngularFirestoreDocument<AmbulanceUser>;
  PractitionerDocument: AngularFirestoreDocument<Practitioner>;

  ROOT_URL: string = 'https://us-central1-salama-1337.cloudfunctions.net/api';

  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache'
  });

  options = {
    headers: this.httpHeaders
  };

  constructor(
    private afs: AngularFirestore,
    private _notification: MessageService,
    private router: Router,
    private http: HttpClient
  ) {}

  //Get users list
  getAmbulanceUsers(): Observable<AmbulanceUser[]> {
    this.AmbulanceUsersCollection = this.afs.collection('ambulanceUsers');
    return this.AmbulanceUsersCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as AmbulanceUser;
        data.identifier.value = action.payload.doc.id;
        return data;
      });
    });
  }

  getPractitioners(): Observable<Practitioner[]> {
    this.PractitionersCollection = this.afs.collection('practitioners');
    return this.PractitionersCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Practitioner;
        data.identifier.value = action.payload.doc.id;
        return data;
      });
    });
  }

  //Get single user
  getAmbulanceUser(id: string): Observable<AmbulanceUser> {
    this.AmbulanceUserDocument = this.afs.doc<AmbulanceUser>(
      `ambulanceUsers/${id}`
    );
    const ambulanceUser = this.AmbulanceUserDocument.snapshotChanges().map(
      action => {
        if (action.payload.exists === false) {
          return null;
        } else {
          const data = action.payload.data() as AmbulanceUser;
          data.identifier.value = action.payload.id;
          return data;
        }
      }
    );
    return ambulanceUser;
  }

  getPractitioner(id: string): Observable<Practitioner> {
    this.PractitionerDocument = this.afs.doc<Practitioner>(
      `practitioners/${id}`
    );
    const practitioner = this.PractitionerDocument.snapshotChanges().map(
      action => {
        if (action.payload.exists === false) {
          return null;
        } else {
          const data = action.payload.data() as Practitioner;
          data.identifier.value = action.payload.id;
          return data;
        }
      }
    );

    return practitioner;
  }

  //Create users for Authentication.
  createUser(email: string, password: string): Observable<any> {
    const data = {
      email: email,
      password: password
    };

    return this.http.post(
      this.ROOT_URL + '/createUser',
      JSON.stringify(data),
      this.options
    );
  }

  // Delete users from Authentication
  deleteUser(id: string): Observable<any> {
    return this.http.delete(this.ROOT_URL + '/users/' + id, this.options);
  }

  // Create User record in database
  createAmbulanceUser(user: AmbulanceUser) {
    return this.http
      .post(
        this.ROOT_URL + '/ambulanceusers',
        JSON.stringify(user),
        this.options
      )
      .subscribe(
        (user: Practitioner) => {
          console.log(user);
          this._notification.create('success', `User Created Successfully`, {
            Title: user.name[0].text.toString(),
            imgURL: user.photo[0].url.toString(),
            Position: 'top-right',
            Style: 'circle',
            Duration: 2000
          });
        },
        e => {
          this._notification.create(
            'danger',
            `User not created. Error. ${e.error.message}`,
            {
              Position: 'top',
              Style: 'bar',
              Duration: 2000
            }
          );
        }
      );
  }

  createPractitioner(user: Practitioner) {
    console.log(user);
    return this.http
      .post(
        this.ROOT_URL + '/practitioners',
        JSON.stringify(user),
        this.options
      )
      .subscribe(
        (user: Practitioner) => {
          console.log(user);
          this._notification.create('success', `User Created Successfully`, {
            Title: user.name[0].text.toString(),
            imgURL: user.photo[0].url.toString(),
            Position: 'top-right',
            Style: 'circle',
            Duration: 2000
          });
        },
        e => {
          this._notification.create(
            'danger',
            `User not created. Error. ${e.error.message}`,
            {
              Position: 'top',
              Style: 'bar',
              Duration: 2000
            }
          );
        }
      );
  }

  //Delete users from database
  deleteAmbulanceUser(id: string): void {
    this.deleteUser(id).subscribe(
      () => {
        this.AmbulanceUserDocument = this.afs.doc<AmbulanceUser>(
          `ambulanceUsers/${id}`
        );
        this.AmbulanceUserDocument.delete()
          .then(() => {
            this._notification.create('danger', `User Deleted Successfully`, {
              Position: 'top',
              Style: 'bar',
              Duration: 2000
            });
            this.router.navigateByUrl('/manage-users');
          })
          .catch(err => {
            this._notification.create(
              'danger',
              `User not deleted. Error ${err.message}`,
              {
                Position: 'top',
                Style: 'bar',
                Duration: 2000
              }
            );
          });
      },
      res => {
        this._notification.create(
          'danger',
          `User not deleted. Error. ${res.error.message}`,
          {
            Position: 'top',
            Style: 'bar',
            Duration: 2000
          }
        );
      }
    );
  }

  deletePractitioner(id: string): void {
    this.deleteUser(id).subscribe(
      () => {
        this.PractitionerDocument = this.afs.doc<Practitioner>(
          `practitioners/${id}`
        );
        this.PractitionerDocument.delete()
          .then(() => {
            this._notification.create('danger', `User Deleted Successfully`, {
              Position: 'top',
              Style: 'bar',
              Duration: 2000
            });
            this.router.navigateByUrl('/manage-users');
          })
          .catch(err => {
            this._notification.create(
              'danger',
              `User not deleted. Error ${err.message}`,
              {
                Position: 'top',
                Style: 'bar',
                Duration: 2000
              }
            );
          });
      },
      res => {
        this._notification.create(
          'danger',
          `User not deleted. Error. ${res.error.message}`,
          {
            Position: 'top',
            Style: 'bar',
            Duration: 2000
          }
        );
      }
    );
  }

  getAmbulanceUserTrips(id: string): Observable<Trip[]> {
    this.TripsCollection = this.afs.collection('trips', ref =>
      ref.where('ambulanceUserId', '==', id)
    );
    const trips = this.TripsCollection.valueChanges();
    return trips;
  }

  getPractitionerAppointments(id: string): Observable<Appointment[]> {
    this.AppointmentsCollection = this.afs.collection('appointments', ref =>
      ref.where('practitionerId', '==', id)
    );
    const appointments = this.AppointmentsCollection.valueChanges();
    return appointments;
  }
}
