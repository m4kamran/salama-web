import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageService } from '../../shared/components/message/message.service';
import Patient from '../models/Patient';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {
  PatientsCollection: AngularFirestoreCollection<Patient>;
  PatientDocument: AngularFirestoreDocument<Patient>;

  constructor(
    private afs: AngularFirestore,
    private _notification: MessageService
  ) {}

  getPatients(): Observable<Patient[]> {
    this.PatientsCollection = this.afs.collection('patients');
    return this.PatientsCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Patient;
        data.identifier.value = action.payload.doc.id;
        return data;
      });
    });
  }

  getPatient(id: string): Observable<Patient> {
    this.PatientDocument = this.afs.doc<Patient>(`patients/${id}`);
    const patient = this.PatientDocument.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Patient;
        data.identifier.value = action.payload.id;
        return data;
      }
    });
    return patient;
  }

  getPractitionerPatients(id: string): Observable<Patient[]> {
    this.PatientsCollection = this.afs.collection('patients', ref =>
      ref.where('practitionerId', '==', id)
    );
    const patients = this.PatientsCollection.valueChanges();
    return patients;
  }

  getPatientsByDate(start: Date, end: Date): Observable<Patient[]> {
    this.PatientsCollection = this.afs.collection('patients', ref =>
      ref
        .where('deceasedDateTime', '>', start)
        .where('deceasedDateTime', '<', end)
    );
    const patients = this.PatientsCollection.valueChanges();
    return patients;
  }

  getIncomingPatients(): Observable<Patient[]> {
    this.PatientsCollection = this.afs.collection('patients', ref =>
      ref.where('status', '==', 'incoming')
    );
    const patients = this.PatientsCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Patient;
        data.identifier.value = action.payload.doc.id;
        return data;
      });
    });
    return patients;
  }

  updatePatient(id: string, patient: Patient) {
    this.afs
      .collection('patients')
      .doc(id)
      .update(patient)
      .then(() => {
        this._notification.create('success', `Patient record updated!`, {
          Position: 'top-right',
          Style: 'bar',
          Duration: 2000
        });
      });
  }
}
