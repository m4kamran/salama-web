import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { MessageService } from '../../shared/components/message/message.service';
import Invoice from '../models/Invoice';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InvoicingService {
  InvoicesCollection: AngularFirestoreCollection<Invoice>;
  InvoicDocument: AngularFirestoreDocument<Invoice>;

  ROOT_URL: string = 'https://us-central1-salama-1337.cloudfunctions.net/api';

  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache'
  });

  options = {
    headers: this.httpHeaders
  };

  constructor(
    private afs: AngularFirestore,
    private _notification: MessageService,
    private router: Router,
    private http: HttpClient
  ) {}

  getInvoices(): Observable<Invoice[]> {
    this.InvoicesCollection = this.afs.collection('invoices');
    return this.InvoicesCollection.snapshotChanges().map(changes => {
      return changes.map(action => {
        const data = action.payload.doc.data() as Invoice;
        data.identifier[0].value = action.payload.doc.id;
        return data;
      });
    });
  }

  getInvoice(id: string): Observable<Invoice> {
    this.InvoicDocument = this.afs.doc<Invoice>(`invoices/${id}`);
    const invoice = this.InvoicDocument.snapshotChanges().map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as Invoice;
        data.identifier[0].value = action.payload.id;
        return data;
      }
    });
    return invoice;
  }

  getPatientInvoices(id: string): Observable<Invoice[]> {
    this.InvoicesCollection = this.afs.collection('invoices', ref =>
      ref.where('recipient', '==', id)
    );
    const invoices = this.InvoicesCollection.valueChanges();
    return invoices;
  }

  createInvoice(invoice: Invoice) {
    console.log(invoice);
    return this.http
      .post(this.ROOT_URL + '/invoices', JSON.stringify(invoice), this.options)
      .subscribe(
        (invoice: Invoice) => {
          console.log(invoice);
          this._notification.create('success', `Invoice Created Successfully`, {
            Title: 'Success',
            Position: 'top-right',
            Style: 'circle',
            Duration: 2000
          });
        },
        e => {
          console.log(e);
          this._notification.create(
            'danger',
            `Invoice not created. Error. ${e.error.message}`,
            {
              Position: 'top',
              Style: 'bar',
              Duration: 2000
            }
          );
        }
      );
    // this.afs
    //   .collection('invoices')
    //   .add(invoice)
    //   .then(() => {
    //     this._notification.create('success', `Invoice Created Successfully`, {
    //       Position: 'top',
    //       Style: 'bar',
    //       Duration: 2000
    //     });
    //   })
    //   .catch(err => {
    //     this._notification.create(
    //       'danger',
    //       `Invoice not created. Error ${err.message}`,
    //       {
    //         Position: 'top',
    //         Style: 'bar',
    //         Duration: 2000
    //       }
    //     );
    //   });
  }

  updateInvoice(id: string, invoice: Invoice): void {
    this.afs
      .collection('invoices')
      .doc(id)
      .update(invoice)
      .then(() => {
        this._notification.create('success', `Invoice Updated Successfully`, {
          Position: 'top-right',
          Style: 'bar',
          Duration: 2000
        });
      })
      .catch(err => {
        this._notification.create(
          'danger',
          `Invoice not updated. Error ${err.message}`,
          {
            Position: 'top',
            Style: 'bar',
            Duration: 2000
          }
        );
      });
  }

  createInvoiceByPatient(id: string, invoice: Invoice) {
    return this.afs
      .collection('invoices')
      .doc(id)
      .set(invoice)
      .then(() => {
        this._notification.create('success', `Invoice Created Successfully`, {
          Position: 'top',
          Style: 'bar',
          Duration: 2000
        });
      })
      .catch(err => {
        this._notification.create(
          'danger',
          `Invoice not created. Error ${err.message}`,
          {
            Position: 'top',
            Style: 'bar',
            Duration: 2000
          }
        );
      });
  }
}
