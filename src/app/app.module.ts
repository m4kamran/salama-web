//Angular Core
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  BrowserModule,
  HammerGestureConfig,
  HAMMER_GESTURE_CONFIG
} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

// Env
import { environment } from '../environments/environment';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

//Routing
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

//Layouts
import { BlankComponent, RootLayout, ExecutiveLayout } from './shared/layouts';
//Layout Service
import { pagesToggleService } from './shared/services/toggler.service';

//Shared Layout Components
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { QuickviewComponent } from './shared/components/quickview/quickview.component';
import { QuickviewService } from './shared/components/quickview/quickview.service';
import { SearchOverlayComponent } from './shared/components/search-overlay/search-overlay.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { HorizontalMenuComponent } from './shared/components/horizontal-menu/horizontal-menu.component';
import { SharedModule } from './shared/components/shared.module';
import { pgListViewModule } from './shared/components/list-view/list-view.module';
import { pgCardModule } from './shared/components/card/card.module';
import { pgCardSocialModule } from './shared/components/card-social/card-social.module';
import { MessageModule } from './shared/components/message/message.module';
import { MessageService } from './shared/components/message/message.service';

//Basic Bootstrap Modules
import {
  BsDropdownModule,
  AccordionModule,
  AlertModule,
  ButtonsModule,
  CollapseModule,
  ModalModule,
  ProgressbarModule,
  TabsModule,
  TooltipModule,
  TypeaheadModule
} from 'ngx-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PopoverModule } from 'ngx-bootstrap';

//Globaly required Components
import { pgTabsModule } from './shared/components/tabs/tabs.module';
import { pgSwitchModule } from './shared/components/switch/switch.module';
import { ProgressModule } from './shared/components/progress/progress.module';
import { pgSelectModule } from './shared/components/select/select.module';

//Thirdparty Components / Plugins
import { QuillModule } from 'ngx-quill';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// Views
import { LoginComponent } from './pages/login/login.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { AngularFireFunctionsModule } from '@angular/fire/functions';
import { ParchiComponent } from './pages/parchi/parchi.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

//Hammer Config Overide
//https://github.com/angular/angular/issues/10541
export class AppHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    pinch: { enable: false },
    rotate: { enable: false }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ExecutiveLayout,
    SidebarComponent,
    QuickviewComponent,
    SearchOverlayComponent,
    HeaderComponent,
    HorizontalMenuComponent,
    BlankComponent,
    RootLayout,
    LoginComponent,
    NotFoundComponent,
    PaymentComponent,
    ParchiComponent
  ],
  imports: [
    pgSelectModule,
    NgxDatatableModule,
    PopoverModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    ProgressModule,
    pgListViewModule,
    pgCardModule,
    pgCardSocialModule,
    RouterModule.forRoot(AppRoutes),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    pgTabsModule,
    PerfectScrollbarModule,
    pgSwitchModule,
    QuillModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    MessageModule
  ],
  providers: [
    QuickviewService,
    pagesToggleService,
    MessageService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: AppHammerConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
