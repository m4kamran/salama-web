import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RootLayout } from '../root/root.component';
import { AuthService } from '../../../core/authentication/auth.service';
import { pagesToggleService } from '../../services/toggler.service';
import { Router } from '@angular/router';
import { MessagingService } from '../../../core/services/messaging.service';
declare var pg: any;

@Component({
  selector: 'executive-layout',
  templateUrl: './executive.component.html',
  styleUrls: ['./executive.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExecutiveLayout extends RootLayout implements OnInit {
  user: any;

  constructor(
    router: Router,
    toggler: pagesToggleService,
    private authService: AuthService,
    private messaging: MessagingService
  ) {
    super(toggler, router);
    this.messaging.getPermission();
    this.messaging.receiveMessage();
  }

  menuItems = [
    {
      label: 'Dashboard',
      routerLink: '/dashboard'
    },
    {
      label: 'Ambulances',
      routerLink: '/ambulances'
    },
    {
      label: 'Patient Records',
      routerLink: '/patient-records'
    },
    {
      label: 'Manage Users',
      routerLink: '/manage-users'
    },
    {
      label: 'Invoices',
      routerLink: '/invoices'
    }
  ];

  ngOnInit() {
    pg.isHorizontalLayout = true;
    this.changeLayout('horizontal-menu');
    this.changeLayout('horizontal-app-menu');

    this.authService.user.subscribe(user => {
      this.user = user;
    });
  }

  signOut() {
    this.authService.signOut();
  }
}
