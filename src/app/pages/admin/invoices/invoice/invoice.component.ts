import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import Invoice from '../../../../core/models/Invoice';
import { InvoicingService } from '../../../../core/services/invoicing.service';
import { P } from '@angular/core/src/render3';
import { PatientsService } from '../../../../core/services/patients.service';
import Patient from '../../../../core/models/Patient';

declare var pg: any;

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  currentInvoice: Invoice;
  patient: Patient;
  status: string;

  isCollapsed = false;
  constructor(
    private route: ActivatedRoute,
    private InvoiceService: InvoicingService,
    private patientsService: PatientsService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.InvoiceService.getInvoice(params.id).subscribe(invoice => {
        this.currentInvoice = invoice;
        this.status = this.currentInvoice.status;
        // this.patientsService
        //   .getPatient(invoice.recipient.toString())
        //   .subscribe((patient: Patient) => {
        //     this.currentInvoice.recipient = patient.name[0].text;
        //   });

        console.log(this.currentInvoice);
      });
    });
  }

  ngAfterViewInit() {
    this.toggleNavbar();
  }

  setFullScreen() {
    pg.setFullScreen(document.querySelector('html'));
  }

  @HostListener('window:resize', [])
  onResize() {
    this.toggleNavbar();
  }

  toggleNavbar() {
    this.isCollapsed = window.innerWidth < 1025;
  }

  getClass(status) {
    if (status.toLowerCase() == 'balanced') return 'text-success';
    else if (status.toLowerCase() == 'issued') return 'text-warning';
    else return 'text-danger';
  }
}
