import { Routes } from '@angular/router';
import { AuthGuard } from '../../../core/guard/auth.guard';
import { ManageInvoicesComponent } from './manage-invoices/manage-invoices.component';
import { InvoiceComponent } from './invoice/invoice.component';

export const AppRoutes: Routes = [
  {
    path: 'invoices',
    component: ManageInvoicesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'invoices/:id',
    component: InvoiceComponent,
    canActivate: [AuthGuard]
  }
];
