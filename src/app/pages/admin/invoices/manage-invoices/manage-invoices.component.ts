import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

import { InvoicingService } from '../../../../core/services/invoicing.service';
import Invoice, { statusTypes } from '../../../../core/models/Invoice';
import { pagesToggleService } from '../../../../shared/services/toggler.service';

import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

declare var pg: any;

@Component({
  selector: 'app-invoices',
  templateUrl: './manage-invoices.component.html',
  styleUrls: ['./manage-invoices.component.scss']
})
export class ManageInvoicesComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('mdSlideUp') mdSlideUp: ModalDirective;

  totalInvoices: number;
  paidInvoices: number;
  cancelledInvoice: number;
  dueInvoices: number;

  statusSort = 'Select Status';

  //Datetable rows
  invoicesRows: Invoice[];

  // Chaching our rows. To restore later.
  invoiceSort: Invoice[];
  isCollapsed = false;

  invoiceForm: FormGroup;
  //No Option YET
  //https://github.com/swimlane/ngx-datatable/issues/423
  scrollBarHorizontal = window.innerWidth < 960;
  columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
  columnModeSettingSmall = window.innerWidth < 560 ? 'standard' : 'force';
  constructor(
    private router: Router,
    private invoiceService: InvoicingService,
    private fb: FormBuilder,
    private toggler: pagesToggleService
  ) {
    window.onresize = () => {
      this.scrollBarHorizontal = window.innerWidth < 960;
      this.columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
      this.columnModeSettingSmall =
        window.innerWidth < 560 ? 'standard' : 'force';
    };
    this.toggler.setContent('');
  }

  ngOnInit() {
    // Populate Datatable
    this.invoiceService.getInvoices().subscribe(invoices => {
      console.log(invoices);
      invoices.map(obj => {
        obj.date = obj.date;
      });

      this.invoicesRows = invoices;
      this.invoiceSort = invoices;

      // Statistics
      this.totalInvoices = invoices.length;

      this.paidInvoices = invoices.filter(x => {
        return x.status == statusTypes.balanced;
      }).length;

      this.dueInvoices = invoices.filter(x => {
        return x.status == statusTypes.issued;
      }).length;

      this.cancelledInvoice = invoices.filter(x => {
        return x.status == statusTypes.cancelled;
      }).length;
    });

    this.invoiceForm = this.fb.group({
      patientId: [''],
      patientName: ['', Validators.required],
      // patientAddress: [],
      treatments: this.fb.array([]),
      amount: [],
      date: ['', Validators.required],
      status: ['issued', Validators.required]
    });
  }

  // Getters
  get patientId() {
    return this.invoiceForm.get('patientId');
  }

  get patientName() {
    return this.invoiceForm.get('patientName');
  }

  get patientAddress() {
    return this.invoiceForm.get('patientAddress');
  }

  get amount() {
    return this.invoiceForm.get('amount');
  }

  get date() {
    return this.invoiceForm.get('date');
  }

  get treatmentsForm() {
    return this.invoiceForm.get('treatments') as FormArray;
  }

  get treatment() {
    return this.invoiceForm.get('treatments').get('treatment');
  }

  get cost() {
    return this.invoiceForm.get('treatments').get('cost');
  }

  get total() {
    const sum = function(items, prop) {
      return items.reduce(function(a, b) {
        return a + parseInt(b[prop]);
      }, 0);
    };

    return sum(this.invoiceForm.value.treatments, 'cost');
  }

  // Form functions
  addTreatment() {
    const treatments = this.fb.group({
      treatment: [],
      cost: []
    });

    this.treatmentsForm.push(treatments);
  }

  deleteTreatment(i) {
    this.treatmentsForm.removeAt(i);
  }

  disabledDate(current: Date): boolean {
    //Future
    return current && current.getTime() < Date.now();
  }

  createInvoice() {
    const sum = function(items, prop) {
      return items.reduce(function(a, b) {
        return a + parseInt(b[prop]);
      }, 0);
    };

    let amount = sum(this.invoiceForm.value.treatments, 'cost');

    this.invoiceForm.get('amount').setValue(amount);
    this.invoiceService.createInvoice(this.invoiceForm.value);
    this.mdSlideUp.hide();
  }

  // Ngx Datatable methods
  markPaid() {
    this.selected.forEach(row => {
      this.invoiceService.updateInvoice(row.identifier[0].value, {
        status: statusTypes.balanced
      });
    });

    this.selected = [];
  }

  markDue() {
    this.selected.forEach(row => {
      this.invoiceService.updateInvoice(row.identifier[0].value, {
        status: statusTypes.draft
      });
    });

    this.selected = [];
  }

  onClick(row) {
    console.log(row);
    this.router.navigate(['invoices/', row.identifier[0].value]);
  }

  selected = [];
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  onActivate(e) {}

  onCheckboxChangeFn($event) {}

  ngAfterViewInit() {
    this.toggleNavbar();
  }
  setFullScreen() {
    pg.setFullScreen(document.querySelector('html'));
  }

  @HostListener('window:resize', [])
  onResize() {
    this.toggleNavbar();
  }

  toggleNavbar() {
    this.isCollapsed = window.innerWidth < 1025;
  }

  updateFilterSearch(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.invoiceSort.filter(function(d) {
      return d.patientName.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.invoicesRows = temp;
    this.table.offset = 0;
  }

  updateFilterStatus(status) {
    if (status == '') {
      this.statusSort = 'All';
    } else this.statusSort = status;

    console.log(this.statusSort);

    status = status.toLowerCase();
    // filter our data
    const temp = this.invoiceSort.filter(function(d) {
      return d.status.toLowerCase().indexOf(status) !== -1 || !status;
    });

    this.invoicesRows = temp;
    this.table.offset = 0;
  }

  // Model
  showInvoiceMoodel() {
    this.mdSlideUp.show();
  }
}
