import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PatientsService } from '../../../../core/services/patients.service';
import Patient from '../../../../core/models/Patient';

@Component({
  selector: 'app-patient-records',
  templateUrl: './patient-records.component.html',
  styleUrls: ['./patient-records.component.scss']
})
export class PatientRecordsComponent implements OnInit {
  @ViewChild('addNewUserModal') addNewUserModal: ModalDirective;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  patientRecords: Patient[];
  patientSort: Patient[];
  statusSort = 'Filter Patients';

  scrollBarHorizontal = window.innerWidth < 960;
  columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
  columnModeSettingSmall = window.innerWidth < 560 ? 'standard' : 'force';

  constructor(private patientService: PatientsService, private router: Router) {
    window.onresize = () => {
      this.scrollBarHorizontal = window.innerWidth < 960;
      this.columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
      this.columnModeSettingSmall =
        window.innerWidth < 560 ? 'standard' : 'force';
    };
  }

  ngOnInit() {
    this.patientService.getPatients().subscribe(patients => {
      this.patientRecords = patients;
      this.patientSort = patients;
    });
  }

  onActivate(event) {
    if (event.type === 'click') {
      console.log(event.row);
      this.router.navigate(['/patient-records', event.row.identifier.value]);
    }
  }

  selected = [];
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    console.log(selected);
  }

  showModal() {
    this.addNewUserModal.show();
  }

  getCellClass(value: string): any {
    return {
      'label-success': value === 'treated',
      'label-warning': value === 'arrived',
      'label-danger': value === 'incoming'
    };
  }

  updateFilterSearch(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.patientSort.filter(function(d) {
      // Change the column name here
      // example d.places
      return d.name[0].text.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.patientRecords = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  updateFilterStatus(status) {
    if (status == '') {
      this.statusSort = 'All';
    } else this.statusSort = status;

    status = status.toLowerCase();

    // filter our data
    const temp = this.patientSort.filter(function(d) {
      // Change the column name here
      // example d.places
      return d.status.toLowerCase().indexOf(status) !== -1 || !status;
    });

    // update the rows
    this.patientRecords = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
