import { Routes } from '@angular/router';
import { AuthGuard } from '../../../core/guard/auth.guard';

import { PatientRecordsComponent } from './patient-records/patient-records.component';
import { PatientComponent } from './patient/patient.component';
import { AmbulancesComponent } from './ambulances/ambulances.component';

export const AppRoutes: Routes = [
  {
    path: 'ambulances',
    component: AmbulancesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'patient-records',
    component: PatientRecordsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'patient-records/:id',
    component: PatientComponent,
    canActivate: [AuthGuard]
  }
];
