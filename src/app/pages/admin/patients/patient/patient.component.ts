import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import Patient from '../../../../core/models/Patient';
import { PatientsService } from '../../../../core/services/patients.service';
import { ActivatedRoute, Router } from '@angular/router';
import Practitioner from '../../../../core/models/Practitioner';
import { UsersService } from '../../../../core/services/users.service';
import { InvoicingService } from '../../../../core/services/invoicing.service';
import { pagesToggleService } from '../../../../shared/services/toggler.service';
import Invoice from '../../../../core/models/Invoice';
import { ModalDirective } from 'ngx-bootstrap/modal';

declare var pg: any;

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  @ViewChild('mdSlideUp') mdSlideUp: ModalDirective;

  patient: Patient;
  registeredUser: any;
  loader = false;
  showInvoiceForm: boolean = true;

  practitioners: Practitioner[];
  practitioner: Practitioner;
  patientInvoices: Invoice[];

  invoice: Invoice;
  invoiceForm: FormGroup;

  scrollBarHorizontal = window.innerWidth < 960;
  columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
  columnModeSettingSmall = window.innerWidth < 560 ? 'standard' : 'force';
  constructor(
    private toggler: pagesToggleService,
    private patientsService: PatientsService,
    private route: ActivatedRoute,
    private userService: UsersService,
    private invoiceService: InvoicingService,
    private fb: FormBuilder,
    private router: Router
  ) {
    window.onresize = () => {
      this.scrollBarHorizontal = window.innerWidth < 960;
      this.columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
      this.columnModeSettingSmall =
        window.innerWidth < 560 ? 'standard' : 'force';
    };
    this.toggler.setContent('');
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.patientsService.getPatient(params.id).subscribe(patient => {
        this.patient = patient;
        console.log(patient);

        if (this.patient.practitionerId) {
          this.userService
            .getPractitioner(this.patient.practitionerId)
            .subscribe(practitioner => {
              this.practitioner = practitioner;
            });
        }

        this.invoiceService
          .getPatientInvoices(patient.identifier.value.toString())
          .subscribe(invoices => {
            this.patientInvoices = invoices;
          });

        this.invoiceForm = this.fb.group({
          patientId: [this.patient.identifier.value, Validators.required],
          patientName: [this.patient.name[0].text, Validators.required],
          // patientAddress: [this.patient.address],
          treatments: this.fb.array([]),
          amount: ['', Validators.pattern('^[0-9]*$')],
          date: ['', Validators.required],
          status: ['issued', Validators.required]
        });
      });
    });

    this.userService.getPractitioners().subscribe(practitioners => {
      this.practitioners = practitioners;
    });
  }

  // Getters
  get patientId() {
    return this.invoiceForm.get('patientId');
  }

  get patientName() {
    return this.invoiceForm.get('patientName');
  }

  get patientAddress() {
    return this.invoiceForm.get('patientAddress');
  }

  get amount() {
    return this.invoiceForm.get('amount');
  }

  get date() {
    return this.invoiceForm.get('date');
  }

  get treatmentsForm() {
    return this.invoiceForm.get('treatments') as FormArray;
  }

  get treatment() {
    return this.invoiceForm.get('treatments').get('treatment');
  }

  get cost() {
    return this.invoiceForm.get('treatments').get('cost');
  }

  get total() {
    const sum = function(items, prop) {
      return items.reduce(function(a, b) {
        return a + parseInt(b[prop]);
      }, 0);
    };

    return sum(this.invoiceForm.value.treatments, 'cost');
  }

  selected = [];
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  assignPractitioner(id, name) {
    this.patientsService.updatePatient(
      this.patient.identifier.value.toString(),
      {
        practitionerId: id,
        practitionerName: name
      }
    );

    this.userService.getPractitioner(id).subscribe(practitioner => {
      this.practitioner = practitioner;
    });
  }

  onActivate(e) {}

  calculateTime() {
    this.loader = true;
    let origin = {
      lat: this.patient.location[0],
      lng: this.patient.location[1]
    };
    this.getDistance(origin);
  }

  getDistance(origin) {
    return new google.maps.DistanceMatrixService().getDistanceMatrix(
      {
        origins: [origin],
        destinations: [{ lat: 33.7182, lng: 73.0605 }],
        travelMode: google.maps.TravelMode.DRIVING
      },
      (results: any) => {
        this.patient.distance = results.rows[0].elements[0].distance.text;
        this.patient.place = results.originAddresses[0];
        this.patient.time = results.rows[0].elements[0].duration.text;
        this.loader = false;
      }
    );
  }

  updatePatient(id: string, patient: Patient) {
    this.patientsService.updatePatient(id, patient);
  }

  // Form functions
  addTreatment() {
    const treatments = this.fb.group({
      treatment: [],
      cost: []
    });

    this.treatmentsForm.push(treatments);
  }

  deleteTreatment(i) {
    this.treatmentsForm.removeAt(i);
  }

  disabledDate(current: Date): boolean {
    //Future
    return current && current.getTime() < Date.now();
  }

  createInvoice() {
    const sum = function(items, prop) {
      return items.reduce(function(a, b) {
        return a + parseInt(b[prop]);
      }, 0);
    };

    let amount = sum(this.invoiceForm.value.treatments, 'cost');

    this.invoiceForm.get('amount').setValue(amount);
    this.invoiceService.createInvoice(this.invoiceForm.value);
    this.showInvoiceForm = !this.showInvoiceForm;
    // .then(() => {
    //   this.patientsService.updatePatient(
    //     this.patient.identifier.value.toString(),
    //     {
    //       invoiceId: this.patient.identifier.value.toString()
    //     }
    //   );

    //   this.invoiceService
    //     .getInvoice(this.patient.invoiceId)
    //     .subscribe(invoice => {
    //       this.invoice = invoice;
    //     });
    // });
  }

  onClick(row) {
    console.log(row);
    this.router.navigate(['invoices/', row.identifier[0].value]);
  }

  showInvoiceMoodel() {
    console.log(this.invoiceForm.value);

    this.mdSlideUp.show();
  }
}
