import { Component, OnInit, ViewChild } from '@angular/core';
import { pagesToggleService } from '../../../../shared/services/toggler.service';
import Patient from '../../../../core/models/Patient';
import { PatientsService } from '../../../../core/services/patients.service';
import { UsersService } from '../../../../core/services/users.service';
import AmbulanceUser from '../../../../core/models/AmbulanceUser';

@Component({
  selector: 'app-ambulances',
  templateUrl: './ambulances.component.html',
  styleUrls: ['./ambulances.component.scss'],
  host: {
    '[class.relative]': 'true'
  }
})
export class AmbulancesComponent implements OnInit {
  zoomLevel = 11;
  center = { lat: 33.6844, lng: 73.0479 };
  disableDefaultUI = true;
  showPatientInfo = false;
  showAmbulanceInfo = false;

  showAmbulances = false;
  showIncomingPatients = true;

  patients: Patient[];
  ambulances: AmbulanceUser[];
  patient: Patient;
  ambulance: AmbulanceUser;

  hospital = [33.7182, 73.0605];

  loader = false;
  styles = [
    {
      featureType: 'water',
      elementType: 'all',
      stylers: [
        {
          hue: '#e9ebed'
        },
        {
          saturation: -78
        },
        {
          lightness: 67
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'landscape',
      elementType: 'all',
      stylers: [
        {
          hue: '#ffffff'
        },
        {
          saturation: -100
        },
        {
          lightness: 100
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'geometry',
      stylers: [
        {
          hue: '#bbc0c4'
        },
        {
          saturation: -93
        },
        {
          lightness: 31
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'poi',
      elementType: 'all',
      stylers: [
        {
          hue: '#ffffff'
        },
        {
          saturation: -100
        },
        {
          lightness: 100
        },
        {
          visibility: 'off'
        }
      ]
    },
    {
      featureType: 'road.local',
      elementType: 'geometry',
      stylers: [
        {
          hue: '#e9ebed'
        },
        {
          saturation: -90
        },
        {
          lightness: -8
        },
        {
          visibility: 'simplified'
        }
      ]
    },
    {
      featureType: 'transit',
      elementType: 'all',
      stylers: [
        {
          hue: '#e9ebed'
        },
        {
          saturation: 10
        },
        {
          lightness: 69
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'administrative.locality',
      elementType: 'all',
      stylers: [
        {
          hue: '#2c2e33'
        },
        {
          saturation: 7
        },
        {
          lightness: 19
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'road',
      elementType: 'labels',
      stylers: [
        {
          hue: '#bbc0c4'
        },
        {
          saturation: -93
        },
        {
          lightness: 31
        },
        {
          visibility: 'on'
        }
      ]
    },
    {
      featureType: 'road.arterial',
      elementType: 'labels',
      stylers: [
        {
          hue: '#bbc0c4'
        },
        {
          saturation: -93
        },
        {
          lightness: -2
        },
        {
          visibility: 'simplified'
        }
      ]
    }
  ];

  constructor(
    private toggler: pagesToggleService,
    private patientsService: PatientsService,
    private userService: UsersService
  ) {}

  ngOnInit() {
    this.toggler.setBodyLayoutClass('no-header');
    this.toggler.setPageContainer('full-height');
    this.toggler.setContent('full-width full-height overlay-footer relative');
    setTimeout(() => {
      this.toggler.toggleFooter(false);
    });

    this.patientsService.getPatients().subscribe(patients => {
      console.log(patients);
      this.patients = patients.filter(x => {
        return x.status == 'incoming';
      });
    });

    this.userService.getAmbulanceUsers().subscribe(ambulances => {
      this.ambulances = ambulances;
    });
  }

  zoomIn() {
    this.zoomLevel++;
  }

  zoomOut() {
    this.zoomLevel--;
  }

  onMarkerInit(marker) {}

  patientDetails(patient: Patient) {
    this.loader = true;
    this.patient = patient;
    this.showPatientInfo = true;
    let origin = {
      lat: patient.location.position.latitude.valueOf(),
      lng: patient.location.position.longitude.valueOf()
    };
    this.getDistance(origin);
  }

  ambulanceInfo(ambulance) {
    this.ambulance = ambulance;
    this.showAmbulanceInfo = true;
  }

  getDistance(origin) {
    return new google.maps.DistanceMatrixService().getDistanceMatrix(
      {
        origins: [origin],
        destinations: [{ lat: 33.7182, lng: 73.0605 }],
        travelMode: google.maps.TravelMode.DRIVING
      },
      (results: any) => {
        this.patient.distance = results.rows[0].elements[0].distance.text;
        this.patient.place = results.originAddresses[0];
        this.patient.time = results.rows[0].elements[0].duration.text;
        this.loader = false;
      }
    );
  }
}
