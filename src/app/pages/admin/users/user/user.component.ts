import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../../../core/services/users.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import Appointment from '../../../../core/models/Appointment';
import Trip from '../../../../core/models/Trip';
import Patient from '../../../../core/models/Patient';
import { PatientsService } from '../../../../core/services/patients.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  currentUser: any;
  userData: any[];

  appointments: Appointment[];
  trips: Trip[];
  patients: Patient[];

  type: string = '';

  //No Option YET
  //https://github.com/swimlane/ngx-datatable/issues/423
  scrollBarHorizontal = window.innerWidth < 960;
  columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
  columnModeSettingSmall = window.innerWidth < 560 ? 'standard' : 'force';
  constructor(
    private route: ActivatedRoute,
    private userService: UsersService,
    private patientsService: PatientsService
  ) {
    window.onresize = () => {
      this.scrollBarHorizontal = window.innerWidth < 960;
      this.columnModeSetting = window.innerWidth < 960 ? 'standard' : 'force';
      this.columnModeSettingSmall =
        window.innerWidth < 560 ? 'standard' : 'force';
    };
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params.type == 'ambulanceUser') {
        this.userService.getAmbulanceUser(params.id).subscribe(user => {
          this.currentUser = user;
        });
        this.userService.getAmbulanceUserTrips(params.id).subscribe(trips => {
          this.trips = trips;
          console.log(this.trips);
        });
      } else if (params.type == 'practitioner') {
        this.userService.getPractitioner(params.id).subscribe(user => {
          this.currentUser = user;
        });

        this.userService
          .getPractitionerAppointments(params.id)
          .subscribe(appointments => {
            this.appointments = appointments;
          });

        this.patientsService
          .getPractitionerPatients(params.id)
          .subscribe(patients => {
            this.patients = patients;
          });
      }

      this.type = params.type;
    });
  }

  deleteUser(id: string): void {
    if (this.type == 'ambulanceUser') {
      this.userService.deleteAmbulanceUser(this.currentUser.uid);
    } else if (this.type == 'practitioner') {
      this.userService.deletePractitioner(this.currentUser.uid);
    }
  }

  onActivate(event) {}
}
