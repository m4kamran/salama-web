//Angular Dependencies
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//Router
import { AppRoutes } from './users.routing';

//Thirdparty Dependencies - table and model
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap';

//Bootstrap Components by ngx-bootstrap
import { ButtonsModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';

//NGX Bootstrap Components
import { CollapseModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

//Pages Components by ace
import { SharedModule } from '../../../shared/components/shared.module';
import { pgSelectModule } from '../../../shared/components/select/select.module';
import { pgSelectfx } from '../../../shared/components/cs-select/select.module';
import { pgUploadModule } from '../../../shared/components/upload/upload.module';
import { pgCardModule } from '../../../shared/components/card/card.module';
import { MessageModule } from '../../../shared/components/message/message.module';
import { MessageService } from '../../../shared/components/message/message.service';
import { ProgressModule } from '../../../shared/components/progress/progress.module';

//Thirdparty components
import { TextMaskModule } from 'angular2-text-mask';
import { QuillModule } from 'ngx-quill';

//Componenets
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { UserComponent } from './user/user.component';

@NgModule({
  imports: [
    MessageModule,
    pgCardModule,
    ProgressModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(AppRoutes),
    NgxDatatableModule,
    ModalModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonsModule.forRoot(),
    pgSelectModule,
    TextMaskModule,
    pgSelectfx,
    pgUploadModule,
    TypeaheadModule.forRoot(),
    QuillModule,
    SharedModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [ManageUsersComponent, UserComponent],
  providers: [MessageService]
})
export class UsersModule {}
