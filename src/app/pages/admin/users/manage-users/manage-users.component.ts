import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../../../core/services/users.service';
import { pagesToggleService } from '../../../../shared/services/toggler.service';
import Practitioner from '../../../../core/models/Practitioner';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {
  @ViewChild('addNewUserModal') addNewUserModal: ModalDirective;

  AmbulanceUsers: any[];
  Practitioners: Practitioner[];

  userForm: FormGroup;
  userType: string;

  // Select User type: Filter options
  selectedOption = { value: 'all' };
  options = [
    { value: 'all', label: 'All' },
    { value: 'ambulanceUsers', label: 'Ambulance Users' },
    { value: 'practitioners', label: 'Practitioners' }
  ];

  constructor(
    private userService: UsersService,
    private fb: FormBuilder,
    private toggler: pagesToggleService
  ) {
    this.toggler.setContent('');
  }

  ngOnInit() {
    this.userService.getAmbulanceUsers().subscribe(users => {
      this.AmbulanceUsers = users;
    });

    this.userService.getPractitioners().subscribe(users => {
      this.Practitioners = users;
    });

    this.userForm = this.fb.group({
      name: ['kamran', Validators.required],
      password: ['123123123', [Validators.required, Validators.minLength(6)]],
      email: ['m4kamran008@gmail.com', [Validators.required, Validators.email]],
      displayPicture: ['', Validators.required]
    });
  }

  get name() {
    return this.userForm.get('name');
  }

  get email() {
    return this.userForm.get('email');
  }

  get password() {
    return this.userForm.get('password');
  }

  addUser() {
    if (this.userType == 'ambulanceUser') {
      this.userService.createAmbulanceUser(this.userForm.value);
    } else if (this.userType == 'practitioner') {
      this.userService.createPractitioner(this.userForm.value);
    }
    this.addNewUserModal.hide();
  }

  showModal() {
    this.addNewUserModal.show();
  }

  //Fileupload
  handleChange(e) {
    if (e.fileList.length > 1) {
      e.fileList.shift();
    }

    this.userForm.patchValue({
      displayPicture: e.file.url
    });
  }
}
