import { Routes } from '@angular/router';
import { AuthGuard } from '../../../core/guard/auth.guard';

import { ManageUsersComponent } from './manage-users/manage-users.component';
import { UserComponent } from './user/user.component';

export const AppRoutes: Routes = [
  {
    path: 'manage-users',
    component: ManageUsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'manage-users/:type/:id',
    component: UserComponent,
    canActivate: [AuthGuard]
  }
];
