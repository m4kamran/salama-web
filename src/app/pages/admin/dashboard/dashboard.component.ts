import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../core/authentication/auth.service';
import Patient from '../../../core/models/Patient';
import { PatientsService } from '../../../core/services/patients.service';
import { UsersService } from '../../../core/services/users.service';
import Practitioner from '../../../core/models/Practitioner';
import { InvoicingService } from '../../../core/services/invoicing.service';
import { statusTypes } from '../../../core/models/Invoice';

declare let d3: any;
declare var pg: any;
declare var google: any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  totalInvoices: number;
  paidInvoices: number;
  cancelledInvoice: number;
  dueInvoices: number;

  totalPractitioners: number;
  onlinePractitioners: number;

  incomingPatientsCount;
  incomingPatients: Patient[];
  practitioners: Practitioner[];

  constructor(
    private patientsService: PatientsService,
    private userService: UsersService,
    private invoiceService: InvoicingService
  ) {}

  ngOnInit() {
    this.patientsService.getIncomingPatients().subscribe(patients => {
      this.incomingPatients = patients;
      this.incomingPatientsCount = patients.length;
      this.incomingPatients.forEach(patient => {
        let origin = {
          lat: patient.location.position.latitude.valueOf(),
          lng: patient.location.position.longitude.valueOf()
        };

        return new google.maps.DistanceMatrixService().getDistanceMatrix(
          {
            origins: [origin],
            destinations: [{ lat: 33.7182, lng: 73.0605 }],
            travelMode: google.maps.TravelMode.DRIVING
          },
          (results: any) => {
            patient.distance = results.rows[0].elements[0].distance.text;
            patient.place = results.originAddresses[0];
            patient.time = results.rows[0].elements[0].duration.text;
          }
        );
      });

      this.invoiceService.getInvoices().subscribe(invoices => {
        this.totalInvoices = invoices.length;

        this.paidInvoices = invoices.filter(x => {
          return x.status == statusTypes.balanced;
        }).length;

        this.dueInvoices = invoices.filter(x => {
          return x.status == statusTypes.issued;
        }).length;

        this.cancelledInvoice = invoices.filter(x => {
          return x.status == statusTypes.cancelled;
        }).length;
      });
    });

    this.userService.getPractitioners().subscribe(users => {
      this.practitioners = users.filter(user => {
        return user.login;
      });

      this.totalPractitioners = users.length;
      this.onlinePractitioners = this.practitioners.length;
    });
  }
}
