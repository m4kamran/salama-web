import { Routes } from '@angular/router';

//Guard
import { AuthGuard } from '../../core/guard/auth.guard';

//Componenets
import { DashboardComponent } from './dashboard/dashboard.component';

export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    loadChildren: './users/users.module#UsersModule'
  },

  {
    path: '',
    loadChildren: './invoices/invoices.module#InvoicesModule'
  },
  {
    path: '',
    loadChildren: './patients/patients.module#PatientsModule'
  }
];
