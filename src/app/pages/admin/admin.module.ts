//Angular Dependencies
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//Router
import { AppRoutes } from './admin.routing';

//Thirdparty Dependencies - table and model
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap';

import { NvD3Module } from 'ngx-nvd3';
import { NgxEchartsModule } from 'ngx-echarts';

import {
  SwiperModule,
  SWIPER_CONFIG,
  SwiperConfigInterface
} from 'ngx-swiper-wrapper';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};
import { NguiMapModule } from '@ngui/map';

//Bootstrap Components by ngx-bootstrap
import { TabsModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap';
import { TypeaheadModule } from 'ngx-bootstrap';

//NGX Bootstrap Components
import { CollapseModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

//Pages Components by ace
import { SharedModule } from '../../shared/components/shared.module';
import { pgSelectModule } from '../../shared/components/select/select.module';
import { pgTagModule } from '../../shared/components/tag/tag.module';
import { pgSwitchModule } from '../../shared/components/switch/switch.module';
import { pgTimePickerModule } from '../../shared/components/time-picker/timepicker.module';
import { pgTabsModule } from '../../shared/components/tabs/tabs.module';
import { pgSelectfx } from '../../shared/components/cs-select/select.module';
import { pgDatePickerModule } from '../../shared/components/datepicker/datepicker.module';
import { pgUploadModule } from '../../shared/components/upload/upload.module';
import { pgCardModule } from '../../shared/components/card/card.module';
import { MessageModule } from '../../shared/components/message/message.module';
import { MessageService } from '../../shared/components/message/message.service';
import { ProgressModule } from '../../shared/components/progress/progress.module';

//Thirdparty components
import { TextMaskModule } from 'angular2-text-mask';
import { QuillModule } from 'ngx-quill';

//Componenets
import { DashboardComponent } from './dashboard/dashboard.component';

//print
import { NgxPrintModule } from 'ngx-print';

@NgModule({
  imports: [
    MessageModule,
    pgCardModule,
    pgTabsModule,
    ProgressModule,
    NvD3Module,
    NgxEchartsModule,
    SwiperModule,
    pgSwitchModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(AppRoutes),
    NgxDatatableModule,
    ModalModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    pgSelectModule,
    pgTagModule,
    TextMaskModule,
    pgSwitchModule,
    pgTimePickerModule,
    pgTabsModule,
    pgSelectfx,
    pgUploadModule,
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    pgDatePickerModule,
    QuillModule,
    SharedModule,
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    NgxPrintModule,
    NguiMapModule.forRoot({
      apiUrl:
        'https://maps.google.com/maps/api/js?key=AIzaSyCmVKqJ8dy3V9J7qa4ETLqsBEJ1tCnhSp8'
    })
  ],
  declarations: [DashboardComponent],
  providers: [
    MessageService,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ]
})
export class AdminModule {}
