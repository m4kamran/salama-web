import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PatientsService } from '../../core/services/patients.service';
import Patient from '../../core/models/Patient';

@Component({
  selector: 'app-parchi',
  templateUrl: './parchi.component.html',
  styleUrls: ['./parchi.component.scss']
})
export class ParchiComponent implements OnInit {
  patient: Patient;

  constructor(
    private route: ActivatedRoute,
    private patientService: PatientsService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.patientService.getPatient(params.get('id')).subscribe(patient => {
        this.patient = patient;
      });
    });
  }
}
