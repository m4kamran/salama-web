import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/authentication/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userName: string;
  password: string;
  constructor(private auth: AuthService, private router: Router) {
    if (this.auth) router.navigate(['/dashboard']);
  }

  ngOnInit() {
    this.userName = '';
    this.password = '';
  }

  onSubmit() {
    this.auth.signIn(this.userName, this.password);
  }
}
