import { Component, OnInit, HostListener } from '@angular/core';
import { Input, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InvoicingService } from '../../core/services/invoicing.service';
import Invoice, { statusTypes } from '../../core/models/Invoice';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../../core/authentication/auth.service';

declare var pg: any;
declare var Stripe;
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  @ViewChild('cardElement') cardElement: ElementRef;

  id: string;
  status: string;
  currentInvoice: Invoice;
  isCollapsed = false;

  stripe;
  card;
  cardErrors;

  confirmation: any;
  loading = false;
  amount;
  // description: string = 'This is description';

  constructor(
    private route: ActivatedRoute,
    private invoiceService: InvoicingService,
    private functions: AngularFireFunctions,
    public auth: AuthService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.invoiceService.getInvoice(this.id).subscribe(invoice => {
        this.currentInvoice = invoice;
        console.log(this.currentInvoice);

        this.amount = this.currentInvoice.totalNet;
        this.status = invoice.status;
      });
      console.log(this.id);
    });

    // this.handler = StripeCheckout.configure({
    //   key: 'pk_test_ynPlDh4eMBJmX7gSJg5K525K',
    //   image: '../../../assets/img/patient.svg',
    //   locale: 'auto',
    //   source: async source => {
    //     this.loading = true;
    //     const user = await this.auth.getUser();
    //     const fun = this.functions.httpsCallable('stripeCreateCharge');
    //     this.confirmation = await fun({
    //       source: source.id,
    //       uid: user.uid,
    //       amount: this.amount
    //     }).toPromise();
    //     this.loading = false;
    //   }
    // });

    this.stripe = Stripe('pk_test_HBu9SwAbwX91fkZWK1BaQBdb00zN3yefzP');
    const elements = this.stripe.elements();

    this.card = elements.create('card');
    this.card.mount(this.cardElement.nativeElement);

    this.card.addEventListener('change', ({ error }) => {
      this.cardErrors = error && error.message;
    });
  }

  // ngAfterViewInit() {
  //   this.toggleNavbar();
  // }
  // setFullScreen() {
  //   pg.setFullScreen(document.querySelector('html'));
  // }

  @HostListener('window:resize', [])
  onResize() {
    this.toggleNavbar();
  }

  toggleNavbar() {
    this.isCollapsed = window.innerWidth < 1025;
  }

  async handleForm(e) {
    e.preventDefault();
    console.log(this.card);

    const { source, error } = await this.stripe.createSource(this.card);

    if (error) {
      // Inform the customer that there was an error.
      this.cardErrors = error.message;
      console.log(error.message);
    } else {
      // Send the token to your server.
      this.loading = true;
      console.log(source.id);

      const user = await this.auth.getUser();

      const stripeCharge = this.functions.httpsCallable('stripeCreateCharge');
      this.confirmation = await stripeCharge({
        source: source.id,
        uid: user.uid,
        amount: this.amount * 100
      }).toPromise();
      this.loading = false;
      this.invoiceService.updateInvoice(this.id, {
        status: statusTypes.balanced
      });
    }
  }

  // googleLogin() {
  //   this.auth.googleSignin();
  // }

  // // Open the checkout handler
  // async checkout(e) {
  //   const user = await this.auth.getUser();
  //   this.handler.open({
  //     name: 'Fireship Store',
  //     description: this.description,
  //     amount: this.amount,
  //     email: user.email
  //   });
  //   e.preventDefault();
  // }

  // // Close on navigate
  // @HostListener('window:popstate')
  // onPopstate() {
  //   this.handler.close();
  // }
}
